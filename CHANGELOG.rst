############
Change Log
############

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Unreleased
********
* CIP-2984 Add handling for pst checkpoint schemas and update ska-tel-model in pyproject.toml/poetry.lock to 1.19.8 for ska-csp-configurescan 5.0
* CIP-3311 Fix version of poetry for ReadtheDocs build to 1.8.0
* CIP-3246 updating CI/CD security scanning
* CIP-3211 change visibilities checkpoint mean phase check to mean delay check
* CIP-1398 adding 100G ethernet checkpoint schema
* CIP-1396 adding resampler delay tracker checkpoint schema
* CIP-2824 add to ReadtheDocs (RTD) and minor CI/make updates
* CIP-2494 mean phase was added to visibilities criteria, a boolean was added to indicate if delays should be plotted for visibilities
* CIP-2636 rename bite_initial_timestamp_time_offset and make visibilities checkpoint criteria required
* CIP-1397 update dct checkpoints schema to align with dct checkpoint changes and rename dct to ddr4_corner_turner
* CIP-2003 update delay model schema as some fields have been renamed and there is a new field supported, publish_lead_time_sec
* CIP-2504 update ska-telmodel to 1.19.1, add gitignore, update pyproject.toml file

0.4.3
*****
* CIP-2511 Move sample_rate_k property from bite_configs_schema.json to cbf_input_data.json, since this property was moved between these corresponding system test parameter files in CIP-2228

0.4.2
*****
* CIP-1982 Updating packet stream repair checkpoint to include validate_registers boolean parameter

0.4.1
*****
* CIP-2003 Add delay model config schema and update validation script to support new delay models layout

0.4.0
*****
* CIP-2257 Update ska-telmodel to version 1.15.0 to upgrade to version 3.0 of the delay model json

0.3.8
*****
* CIP-2103 add visibilities checkpoint properties for capture duration and plotting baselines, and add number of timestamps and baselines to criteria

0.3.7
*****
* CIP-2228 Location of sample_rate_k parameter in ska-mid-cbf-system-tests test parameter files has changed; update schemas so that sample_rate_k is valid, and required, in cbf_input_data.json
  rather than in bite_configs.json.

0.3.6
*****
* CIP-2007 Add DCT checkpoint

0.3.5
*****
* CIP-2098 Update bite_configs schema so that only valid k-values can be used to configure BITE

0.3.4
*****
* CIP-2067 Update telmodel to version 1.11.2 to change epoch from int to float

0.3.3
*****
* CIP-1770 vis mesh criteria added to slim_links checkpoint 

0.3.2
*****
* CIP-1770 slim_links checkpoint added with fs mesh criteria

0.3.1
*****
* CIP-1764 Increase telmodel validation strictness from 1 to 2

0.3.0
*****
* CIP-1764 Add validation of the Mid.CBF InitSysParams command against the telescope model.
