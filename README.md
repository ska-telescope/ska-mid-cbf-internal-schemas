# SKA Mid CBF Internal Schemas
This repository is used for managing shared internal schemas for the [CBF signal chain verification test parameters](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/tree/main/test_parameters), as well as validating test parameters against both internal schemas and the [telescope model schemas](https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/ska-csp.html).

Documentation on the Developer's portal:
[ReadTheDocs](https://developer.skao.int/projects/ska-mid-cbf-internal-schemas/en/latest/)

The following test parameters are validated against the internal schemas:
- bite_configs
- filters
- cbf_input_data
- checkpoints
- delay_model_package
- delay_model_config
- scan
- tests

And these parameters are validated against the telescope model schemas:
- assign_resources
- configure_scan
- individual delay models (nested inside the delay_model_package and are automatically validated when the delay_model_package is validated)
- scan commands (nested inside the scan and are automatically validated when the scan is validated)
- release_resources

Currently, validation includes:
- A consistency check to ensure that the test parameters given in a higher-level json file are defined in their respective lower-level json files (e.g., in tests.json, the specified "cbf_input_data" exists in cbf_input_data.json).
- Checking that the test parameters listed above are compliant to their corresponding internal or telescope model schemas.
- IMPORTANT: test_schema.json intentionally disallows 'tests' which contain spaces in their names. For example, the test id "Test_1" is allowed while "Test 1" is not. This is because these test ids are directly used as command-line arguments and as parts of filenames.

## How to run test parameter validation
In the current use case, this repository is exported to CAR and added as a package dependency in the following repositories to run validation inside them:
- [ska-mid-cbf-system-tests](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests)

Validation can also be run directly from the command line to validate new test parameters without running the entire BDD test.

### Validating one or all tests
#### Via exported package, from inside another repository
* Refer to the [Signal Chain Verification BDD Test](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/blob/main/tests/test_mid_cbf_signal_chain_verification.py) for an example

#### From the command line
1. Clone this repo and cd into the project-level directory from the command line
2. Create a Python virtual environment and install the dependencies (if you already have a virtual environment created, skip the first three lines):

Note: make sure you create your virtual environment with the python version required for the packages. For example, we currently need python3.10 (or newer) in order to use the latest schema validation that was released as part of ska-telmodel 1.10.0. If you get an error regarding your schema not matching the telmodel, reinstall the package to make sure you have the latest versions. You can do this by deleting your existing venv and repeating the steps below to reinstall the requirements. 

```
cd ~
pip install virtualenv
python3.10 -m venv venv
source ~/venv/bin/activate
cd .../ska-mid-cbf-internal-schemas 
pip install -r requirements.txt
```
3. Copy the path to a `test_parameters` directory which contains the test parameters you want to validate. Note that this `test_parameters` directory must maintain the same file structure as [ska-mid-cbf-system-tests/test_parameters](https://gitlab.com/ska-telescope/ska-mid-cbf-system-tests/-/tree/main/test_parameters)
4. Cd into this project's `test_parameters` directory and run `./test_parameters_validation.py <copied_path> -t <test_id>`*.

*The argument `-t <test_id>` is optional - if included, validate a single test by specifying the `test_id` (omitting the `test_id` validates all tests).

### Validating a single test parameter against its schema (command line only)
1. Again, you will need to be inside a virtual environment (see above instruction to create one)
2. Copy the path to the test parameter json file to be validated
3. Cd into this project's `test_parameters` directory and run `./test_parameters_validate_schema_only.py <parameter_name> <copied_path>`.*

*The `<parameter_name>` arg can be one of the following options: `['assign_resources', 'bite_configs', 'cbf_input_data', 'checkpoints', 'configure_scan', 'delay_model_package', 'delay_model_config', 'filters', 'release_resources', 'scan', 'tests']`