#include PrivateRules.mak
include .make/*.mk

PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E501,F407,W503,D100,D103,D400,DAR101,D104,D101,D107,D401,FS002,D200,DAR201,D202,D403,DAR401
PYTHON_SWITCHES_FOR_PYLINT = --disable=W0613,C0116,C0114,R0801,W0621,C0301,F0010,W0511
PYTHON_LINT_TARGET = test_parameters/

create-python-virtualenv:
	python3.9 -m venv venv

requirements:
	poetry install

run-pylint:
	pylint --output-format=parseable test_parameters/ | tee build/code_analysis.stdout
