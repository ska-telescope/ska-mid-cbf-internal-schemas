#!/usr/bin/env python3

import argparse
import os
import sys

from jsonschema import validate
from test_parameters_validation import (
    LOCAL_SCHEMA_EXISTS_FOR_TEST_PARAMETER,
    get_json,
    validate_local_config,
    validate_telmodel_config,
)

sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

# Directory containing the the schemas
SCHEMAS_DIR = os.path.dirname(os.path.abspath(__file__))
TESTS_SCHEMA_FILE = os.path.join(SCHEMAS_DIR, "tests_schema.json")
BITE_CONFIGS_SCHEMA_FILE = os.path.join(
    SCHEMAS_DIR,
    "cbf_input_data",
    "bite_config_parameters",
    "bite_configs_schema.json",
)
FILTERS_SCHEMA_FILE = os.path.join(
    SCHEMAS_DIR,
    "cbf_input_data",
    "bite_config_parameters",
    "filters_schema.json",
)
DELAY_MODEL_PACKAGE_SCHEMA_FILE = os.path.join(
    SCHEMAS_DIR, "delay_model_package", "delay_model_package_schema.json"
)

EXISTING_SCHEMAS = [
    "assign_resources",
    "bite_configs",
    "cbf_input_data",
    "checkpoints",
    "configure_scan",
    "delay_model_package",
    "delay_model_config",
    "filters",
    "release_resources",
    "scan",
    "tests",
    "init_sys_param",
]


def validate_telmodel_configs(parameter, configs):
    """validates the given test parameter against its telescope model schema

    Args:
    - parameter: the test parameter to validate
    - configs: the parameter configurations to validate
    """
    print(f"Validating '{parameter}' against the ska-telmodel schema...")
    for config_id, config in configs.items():
        print(f"  - Validating '{config_id}'...")
        validate_telmodel_config(parameter, config)
        print(f"  - '{config_id}' passed validation.")
    print(f"'{parameter}' passed validation.")


def validate_test_parameter_against_schema(parameter, file):
    """validates the given test parameter against its schema

    Args:
    - parameter: the test parameter to validate
    - file: the file containing the parameter configurations
    """
    config = get_json(file)
    # Validate telescope model parameters against the telescope model schema
    if (
        parameter in LOCAL_SCHEMA_EXISTS_FOR_TEST_PARAMETER
        and not LOCAL_SCHEMA_EXISTS_FOR_TEST_PARAMETER[parameter]
    ):
        validate_telmodel_configs(parameter, config[parameter])

    elif parameter == "scan":
        print(
            f"Validating '{parameter}' against {parameter}_schema.json and the ska-telmodel schema..."
        )
        for scan_id, scan in config[parameter].items():
            print(
                f"  - '{scan_id}': Validating scan command against the ska-telmodel schema..."
            )
            validate_telmodel_config("scan", scan["command"])
            print(f"  - '{scan_id}': Scan command passed validation.")
        validate(
            instance=config,
            schema=get_json(
                os.path.join(
                    SCHEMAS_DIR, parameter, f"{parameter}_schema.json"
                )
            ),
        )
        print(f"'{parameter}' passed validation.")

    # Validate delay model packages and delay models against the local and telescope model schemas, respectively
    elif parameter == "delay_model_package":
        print(
            f"Validating '{parameter}' against {parameter}_schema.json and the ska-telmodel schema..."
        )
        description = config["description"]
        for delay_model in config["models"]:
            validate_telmodel_config("delay_models", delay_model["model"])
        print(f"  - '{description}': Delay models passed validation.")
        validate(
            instance=config,
            schema=get_json(DELAY_MODEL_PACKAGE_SCHEMA_FILE),
        )
        print(f"'{parameter}' passed validation.")

    # Validate local test parameters against the local schema
    else:
        if parameter == "tests":
            schema = get_json(TESTS_SCHEMA_FILE)

        elif parameter == "bite_configs":
            schema = get_json(BITE_CONFIGS_SCHEMA_FILE)

        elif parameter == "filters":
            schema = get_json(FILTERS_SCHEMA_FILE)

        else:
            schema = get_json(
                os.path.join(
                    SCHEMAS_DIR, parameter, f"{parameter}_schema.json"
                )
            )

        validate_local_config(parameter, config, schema)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        dest="parameter",
        choices=EXISTING_SCHEMAS,
        help=f"parameter to validate. Options: one of {EXISTING_SCHEMAS}",
    )

    parser.add_argument(
        dest="test_param_file", help="file containing the test parameter"
    )

    args = parser.parse_args()
    validate_test_parameter_against_schema(
        args.parameter, args.test_param_file
    )
