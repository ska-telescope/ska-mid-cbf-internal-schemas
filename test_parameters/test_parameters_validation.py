#!/usr/bin/env python3

import argparse
import os
import sys

import jsonref
from jsonschema import ValidationError, validate
from ska_telmodel.schema import validate as telmodel_validate

sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

# Directory containing the schemas
SCHEMAS_DIR = os.path.dirname(os.path.abspath(__file__))
BITE_CONFIG_SCHEMA_FILE = os.path.join(
    SCHEMAS_DIR,
    "cbf_input_data",
    "bite_config_parameters",
    "bite_configs_schema.json",
)
FILTERS_SCHEMA_FILE = os.path.join(
    SCHEMAS_DIR,
    "cbf_input_data",
    "bite_config_parameters",
    "filters_schema.json",
)


LOCAL_SCHEMA_EXISTS_FOR_TEST_PARAMETER = {
    "cbf_input_data": True,
    "delay_model_config": True,
    "checkpoints": True,
    "assign_resources": False,
    "configure_scan": False,
    "scan": True,
    "release_resources": False,
    "init_sys_param": False,
}


def get_json(file):
    """parses the JSON file and returns a dict containing the file's contents

    Args:
    - file: the JSON file to parse
    Returns:
    - a dict containing the contents of the JSON file
    """
    with open(f"{file}", mode="r", encoding="utf-8") as config_fd:
        config = jsonref.load(config_fd)
    return config


def validate_local_config(parameter, config, schema):
    """validates the given test parameter against its local schema

    Args:
    - parameter: the test parameter to validate
    - config: the dict containing all the configurations for the parameter
    - schema: the schema corresponding to the parameter
    """
    print(f"Validating '{parameter}' against {parameter}_schema.json...")
    try:
        validate(instance=config, schema=schema)
    except ValidationError as validation_err:
        print("Validation failed.")
        if parameter == "tests":
            print("Confirm that the test id names contain no spaces.")
        raise validation_err
    print(f"'{parameter}' passed validation.")


def validate_filters(filters_file, filters):
    """validates the filters against the local schema

    Args:
    - filters_file: the file containing the filter definitions
    - filters: the filters to validate
    """
    filter_configs = get_json(filters_file)["filters"]
    filter_schema = get_json(FILTERS_SCHEMA_FILE)["properties"]["filters"][
        "patternProperties"
    ][".*"]

    for filter_id in filters:
        print(
            f"        - {os.path.basename(filters_file)}: Validating '{filter_id}' against consistency check and {os.path.splitext(os.path.basename(filters_file))[0]}_schema.json..."
        )
        try:
            filter_config = filter_configs[filter_id]
        except Exception:
            print(f"Error: '{filter_id}' not found in {filters_file}")
            raise
        validate(instance=filter_config, schema=filter_schema)
        print(
            f"        - {os.path.basename(filters_file)}: '{filter_id}' passed validation."
        )


def validate_bite_configs(test_params_dir, bite_config_file, receptors):
    """validates the BITE configurations against the local schema

    Args:
    - test_params_dir: the top-level directory containing the test parameters
    - bite_config_file: the file containing the BITE configs definitions
    - receptors: the list of receptors which contain the BITE config ids to validate
    """
    bite_configs = get_json(bite_config_file)
    bite_config_schema = get_json(BITE_CONFIG_SCHEMA_FILE)["properties"][
        "bite_configs"
    ]["patternProperties"][".*"]

    bite_config_ids = set()
    for receptor in receptors:
        bite_config_id = receptor["bite_config_id"]
        if bite_config_id not in bite_config_ids:
            bite_config_ids.add(bite_config_id)
            print(
                f"      - {os.path.basename(bite_config_file)}: Validating '{bite_config_id}' against consistency check and {os.path.splitext(os.path.basename(bite_config_file))[0]}_schema.json..."
            )
            try:
                bite_config = bite_configs["bite_configs"][bite_config_id]
            except Exception:
                print(
                    f"Error: '{bite_config_id}' not found in {bite_config_file}"
                )
                raise

            filters = set()
            for source in bite_config["sources"]:
                filter_x = source["gaussian"]["pol_x"]["filter"]
                filter_y = source["gaussian"]["pol_y"]["filter"]
                filters.add(filter_x)
                filters.add(filter_y)

            filters_file = os.path.join(
                test_params_dir,
                "cbf_input_data",
                "bite_config_parameters",
                "filters.json",
            )
            validate_filters(filters_file, filters)

            validate(
                instance=bite_configs["bite_configs"][bite_config_id],
                schema=bite_config_schema,
            )
            print(
                f"      - {os.path.basename(bite_config_file)}: '{bite_config_id}' passed validation."
            )


def validate_telmodel_config(parameter, config):
    """validates a single test parameter configuration against the telescope model

    Args:
    - parameter: the parameter to validate, e.g., 'configure_scan'
    - config: the dict to validate against the telescope model schema for the given parameter
    """
    try:
        telmodel_validate(
            version=config["interface"], config=config, strictness=2
        )
    except Exception as ex:
        print(
            f"ERROR: '{parameter}' validation against ska-telmodel schema failed with the follow exception:\n{ex}"
        )
        raise


def validate_single_parameter(
    test_params_dir, parameter, config_id, local_schema_exists
):
    """validates a single test parameter configuration against its local or telescope model schema

    Args:
    - test_params_dir: the top-level directory containing the test parameters
    - parameter: the parameter to validate, e.g., 'delay_model_package'
    - config_id: the id mapping to the specific test parameter to validate, e.g., 'Single zero-value model'
    - local_schema_exists: whether a local schema exists for the specified parameter
    """
    if local_schema_exists:
        schema_file = os.path.join(
            SCHEMAS_DIR, parameter, f"{parameter}_schema.json"
        )
        schema = get_json(schema_file)

    filename = f"{parameter}.json"
    file = os.path.join(test_params_dir, parameter, filename)
    all_configs = get_json(file)

    try:
        config = all_configs[parameter][config_id]
    except Exception:
        print(f"Error: '{config_id}' not found in {file}")
        raise

    if local_schema_exists:
        print(
            f"  - {os.path.basename(filename)}: Validating '{config_id}' against consistency check and {os.path.basename(schema_file)}..."
        )
        if parameter == "cbf_input_data":
            bite_config_file = os.path.join(
                test_params_dir,
                "cbf_input_data",
                "bite_config_parameters",
                "bite_configs.json",
            )
            validate_bite_configs(
                test_params_dir, bite_config_file, config["receptors"]
            )
        elif parameter == "delay_model_config":
            dm_package_file = os.path.join(
                test_params_dir, "delay_model_package", config["source_file"]
            )
            delay_models = get_json(dm_package_file)
            print(
                f"      - '{config_id}': Validating delay models against the ska-telmodel schema..."
            )
            for delay_model in delay_models["models"]:
                validate_telmodel_config("delay_models", delay_model["model"])
            print(f"      - '{config_id}': Delay models passed validation.")
        elif parameter == "scan":
            print(
                f"      - '{config_id}': Validating scan command against the ska-telmodel schema..."
            )
            validate_telmodel_config("scan", config["command"])
            print(f"      - '{config_id}': Scan command passed validation.")

        config_schema = schema["properties"][parameter]["patternProperties"][
            ".*"
        ]

        validate(instance=config, schema=config_schema)
        print(
            f"  - {os.path.basename(filename)}: '{config_id}' passed validation."
        )

    else:
        print(
            f"  - {os.path.basename(filename)}: Validating '{config_id}' against consistency check and ska-telmodel schema..."
        )
        validate_telmodel_config(parameter, config)
        print(
            f"  - {os.path.basename(filename)}: '{config_id}' passed validation."
        )


def validate_single_test(test_params_dir, test_id, test_params, test_schema):
    """validates the test parameters corresponding to a specified test id

    Args:
    - test_params_dir: the top-level directory containing the test parameters
    - test_id: the test id to validate
    - test_params: the dict which maps each test parameter to their respective id for the given test id
    - test_schema: the schema to validate the test id against
    """
    print(f"Validating '{test_id}'...")

    for (
        parameter,
        local_schema_exists,
    ) in LOCAL_SCHEMA_EXISTS_FOR_TEST_PARAMETER.items():
        # init_sys_param is not part of the parameterized test cases, so skip its validation for each test id
        if parameter != "init_sys_param":
            config_id = test_params[parameter]
            validate_single_parameter(
                test_params_dir, parameter, config_id, local_schema_exists
            )

    validate(instance=test_params, schema=test_schema)
    print(f"'{test_id}' passed validation.")


def validate_init_sys_param(test_params_dir):
    # Load the init_sys_param.json file
    try:
        init_sys_param_json = get_json(
            os.path.join(test_params_dir, "init_sys_param.json")
        )
    except Exception as ex:
        print(
            f"ERROR: loading init_sys_param.json from {test_params_dir} failed with the following exception:\n{ex}"
        )
        raise

    # Run validation against the telmodel
    for init_sys_param_id, init_sys_param_config in init_sys_param_json[
        "init_sys_param"
    ].items():
        validate_telmodel_config("init_sys_param", init_sys_param_config)
        print(f"init_sys_param ID '{init_sys_param_id}' passed validation.")
    print("init_sys_param.json passed validation.")


def validate_test_parameters(test_params_dir, test_id=None):
    """validates the test parameters for all test ids, or the specified test id

    Args:
    - test_params_dir: the top-level directory containing the test parameters
    - (optional) test_id: the single test id to validate. By default, all test ids will be validated
    """
    # Validate the init_sys_param.json
    validate_init_sys_param(test_params_dir)

    # Validate the parameterized tests
    try:
        tests = get_json(os.path.join(test_params_dir, "tests.json"))
    except Exception as ex:
        print(
            f"ERROR: could not load json from test parameters directory '{test_params_dir}'. The following exception occurred:\n{ex}"
        )
        raise

    tests_schema = get_json(os.path.join(SCHEMAS_DIR, "tests_schema.json"))
    validate_local_config("tests", tests, tests_schema)
    single_test_schema = tests_schema["properties"]["tests"][
        "patternProperties"
    ]["^\\S*$"]

    if test_id is None:
        for test_id_, test_params in tests["tests"].items():
            validate_single_test(
                test_params_dir, test_id_, test_params, single_test_schema
            )
    else:
        validate_single_test(
            test_params_dir,
            test_id,
            tests["tests"][test_id],
            single_test_schema,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        dest="test_params_dir",
        help="test parameters directory",
    )

    parser.add_argument(
        "-t",
        dest="test_id",
        nargs="?",
        default=None,
        help="test id",
    )

    args = parser.parse_args()
    validate_test_parameters(
        test_params_dir=args.test_params_dir, test_id=args.test_id
    )
